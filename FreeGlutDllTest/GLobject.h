#pragma once
#include <string>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/SOIL.h>
#include "GLShader.h"

class GLobject
{
	GLushort * indeces;

	GLobject();

public:
	GLuint VBO, VAO, IBO, texture;
	GLsizei stride, CoordOffset, NormalOffset, TexCoordOffset, ColorOffset;
	glm::mat4 object_transformation;

	glm::vec4 material_ambient, material_diffuse, material_specular, material_emission;
	GLfloat material_shininess;
	bool use_texture;

	int count_vertex;
	int count_indexes;
	GLobject(std::string path, std::string pathtex, glm::vec3 clr = { 1,1,1 }, glm::vec3 norm = { 0,0,0 });


	bool BindAttributesToShader(GLShader& shaderobject);

	void drawObject();

	~GLobject();

};